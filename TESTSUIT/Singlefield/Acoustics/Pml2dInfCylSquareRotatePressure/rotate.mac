!/NOPR
!
! rotv.mac a macro to rotate the currently selected volumes
! about an axis.
!
! usage: rotate,entitytype,x1,y1,z1,x2,y2,z2,rotangle,flag,coord,gran
!
! where: x1,y1,z1=the coordinates of a point along the rotation axis
! x2,y2,z2=the coordinates of another point along the rotation axis
! rotangle=rotation angle in degrees using right hand rule
! flag =0 for move, 1 for copy
! coord =0 for global coordinate system, 1 for local coordinate system
! (coordinate system the two points are described in.)
! gran =1 if this macro is called from a granule
! (activates the component commands to retrieve the originally
! selected volumes stored in the component _Y1.)
!
! the granule has already stored the originally selected volumes in the
! component _Y1. This component is retrieved and added to the set of
! volumes used in the macro. The component is then deleted.
!
! written by John Crawford 4-30-96
!
! variable definition
! ar20=csid coordinate system ID active in the original model
! ar21=kpmax max keypoint ID in the original model
! ar22=lsmax max line number used in the original model
! ar23=csmax max coordinate system number used in the original model
!
!*msg,info,arg2,arg3,arg4,arg5,arg6,arg7,arg8,arg9,arg20
!rotv.mac %g %g %g %g %g %g %g %i %i %i
! check to make sure that the distance between location 1 and location 2 is
! nonzero
*IF,((arg2-arg5)**2+(arg3-arg6)**2+(arg4-arg7)**2),GT,0,THEN
  !
  ! the rotation macro begins here
  !
  ! save the view data
  /GSAVE,_Y1
  !  set the screen distance, etc., using /user
  /USER
  IMME,OFF
  /UIS,REPLOT
  !
  ! get the current coordinate system ID number
  *GET,ar20,ACTIVE,,CSYS
  !
  ! get the highest coordinate system ID in the model
  ar23=CSYIQR(0,14)
  ! get the highest keypoint number in the model
  ar21=KPINQR(0,14)
  !
  ! create a local coordinate system at the work plane location, so we can
  ! return the work plane to it's original location when the macro is finished
  ! running. it is given an ID number which is 11 greater than the previously
  ! highest ID number.
  CSWPLA,ar23+11,0
  !
  ! create the local cylindrical coordinate system using coordinates 1 and 2
  ! by creating three temporary keypoints at these locations.
  !
  ! choose the coordinate system in which keypoints will be generated
  *IF,arg20,EQ,0,THEN
    CSYS
  *ELSE
    CSYS,ar20
  *ENDIF
  !
  K,ar21+1,arg2,arg3,arg4
  K,ar21+2,arg5,arg6,arg7
  !
  ! return to the temporary local coordinate system
  CSYS,ar23+11
  !
  ! get the highest line ID which exists, and then set numst,line to this plus 1
  ar22=LSINQR(0,14)
  NUMSTR,LINE,ar22+1
  !
  ! create a line between these two points
  L,ar21+1,ar21+2
  !
  ! align the working plane to this location at point 1
  LWPLAN,-1,ar22+1
  !
  ! remove the line
  LDEL,ar22+1
  !
  ! delete the temporary keypoints we just made
  KDEL,ar21+1,ar21+2
  !
  ! reset numstr for lines and keypoints
  NUMSTR,LINE,ar22+1
  NUMSTR,KP,ar21+1
  !
  ! create local coordinate system which is aligned with the work plane
  CSWPLA,ar23+12,1
  !
  !imme,on
  ! do an lgen on the selected volumes
  *IF,LWCASE(STRCOMP(arg1)),EQ,'line',THEN
    *IF,arg9,EQ,0,THEN
      ! if moving, then do this
      LGEN,,ALL,,,,arg8,,,1,1
    *ELSEIF,arg9,EQ,1,THEN
      ! if copying, then do this
      LGEN,2,ALL,,,,arg8,,,,0
    *ENDIF
  *ELSEIF,LWCASE(STRCOMP(arg1)),EQ,'area',THEN
    *IF,arg9,EQ,0,THEN
      ! if moving, then do this
      AGEN,,ALL,,,,arg8,,,1,1
    *ELSEIF,arg9,EQ,1,THEN
      ! if copying, then do this
      AGEN,2,ALL,,,,arg8,,,,0
    *ENDIF      
  *ELSEIF,LWCASE(STRCOMP(arg1)),EQ,'volume',THEN
    *IF,arg9,EQ,0,THEN
      ! if moving, then do this
      VGEN,,ALL,,,,arg8,,,1,1
    *ELSEIF,arg9,EQ,1,THEN
      ! if copying, then do this
      VGEN,2,ALL,,,,arg8,,,,0
    *ENDIF
  *ELSE,LWCASE(STRCOMP(arg1)),EQ,'test',THEN
    *MSG,INFO
*** You did not specify a correct entity string ***
  *ENDIF
  !
  ! return the work plane to it's original location
  WPCSYS,-1,ar23+11
  ! return to the original coordinate system
  CSYS,ar20
  !
  ! delete the temporary coordinate systems
  CSDELE,ar23+11
  CSDELE,ar23+12
  !
  ! additionally select the originally selected volumes
  *IF,ar10,EQ,1,THEN
    CMSEL,A,_Y1
  *ENDIF
  !
*ELSE
  *MSG,INFO
*** Coordinates for rotation axis are coincident ***
  *MSG,INFO
******** Re-enter coordinates and try again ********
*ENDIF
!
*IF,ar10,EQ,1,THEN
  ! additionally select the originally selected volumes
  CMSEL,A,_Y1
  !
  ! delete the temporary component
  CMDELE,_Y1
*ENDIF
!
/PLOPT,WP,OFF
/GRESUME,_Y1
/SYS,rm -f _Y1
! turn the working plane display symbol off
WPSTYL,,,,,,,,0
*IF,LWCASE(STRCOMP(arg1)),EQ,'line',THEN
  LPLO
*ELSEIF,LWCASE(STRCOMP(arg1)),EQ,'area',THEN
  APLO
*ELSEIF,LWCASE(STRCOMP(arg1)),EQ,'volume',THEN
  VPLO
*ENDIF
!/GOPR
