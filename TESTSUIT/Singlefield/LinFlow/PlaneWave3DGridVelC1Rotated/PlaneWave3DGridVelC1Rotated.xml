<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.cfs++.org/simulation">
  
  <documentation>
    <title>Transient LinFlow-PDE including the first convective term caused by the ALE-framework</title>
    <authors>
      <author>fspoerk</author>
    </authors>
    <date>2022-07-20</date>
    <keywords>
      <keyword>flow</keyword>
      <keyword>output</keyword>
    </keywords>
    <references> 
      Non Just functionality test
    </references>
    <isVerified>yes</isVerified>
    <description>
      1D problem (plane wave in a channel) with pressure excitation. Due to the prescribed constant grid velocity, a simple analytical solution can be found for the case µ=0.
      We compare the resulting wave number with the analytical one given by
      k = \omega*(v_g+\sqrt(v_g^2+4*c_0^2))/(2*c_0^2)
      In this testcase the channel is 3 dimensional and rotated, hence, we have to use the weak noPenetration BC instead of the standard slip BC. 
      For verification the 3D test is compared to the 2D model by displaying them side by side with paraview as well as checking if the 3D solution corresponds to the 2D solution with matlab.
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <hdf5 fileName="PlaneWave3DGridVelC1Rotated.h5ref"/>
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="3d">
    <variableList>
      <var name="fx" value="0.5"/>
      <var name="fy" value="0"/>
      <var name="fz" value="0"/>
      <var name="ax" value="pi/3"/>
      <var name="ay" value="pi/6"/>
      <var name="az" value="pi/3"/>
      <!-- Punkt (fx,fy,fz) um x-Achse gedreht -->
      <var name="x_x" value="fx"/>
      <var name="y_x" value="fy*cos(ax)-fz*sin(ax)"/>
      <var name="z_x" value="fy*sin(ax)+fz*cos(ax)"/>
      <!-- Punkt (fx,fy,fz) um x- und y-Achse gedreht -->
      <var name="x_xy" value="x_x*cos(ay)+z_x*sin(ay)"/>
      <var name="y_xy" value="y_x"/>
      <var name="z_xy" value="z_x*cos(ay)-x_x*sin(ay)"/>
      <!-- Punkt (fx,fy,fz) um xyz gedreht -->
      <var name="x_xyz" value="x_xy*cos(az)-y_xy*sin(az)"/>
      <var name="y_xyz" value="x_xy*sin(az)+y_xy*cos(az)"/>
      <var name="z_xyz" value="z_xy"/>
    </variableList>
    
    <regionList>
      <region name="Channel" material="FluidMat"/>
    </regionList>
    
    <surfRegionList>
      <surfRegion name="Excite"/>
      <surfRegion name="BC_Bot"/>
      <surfRegion name="BC_Top"/>
      <surfRegion name="BC_End"/>
      <surfRegion name="BC_Right"/>
      <surfRegion name="BC_Back"/>
    </surfRegionList> 
    
    <nodeList>
      <nodes name="S1">
        <coord x="0" y="0" z="0"/>
      </nodes>
      <nodes name="S2">      
        <coord x="x_xyz" y="y_xyz" z="z_xyz"/>
      </nodes>
    </nodeList>
  </domain>
  
  <fePolynomialList>
    <!-- Set second order polynomial for velocity -->
    <Lagrange id="orderVel">
      <isoOrder>2</isoOrder> 
    </Lagrange>
    
    <!-- Set first order polynomial for pressure -->
    <Lagrange id="orderPres">
       <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>
  
  <sequenceStep>
    <analysis>
      <transient>
        <numSteps>50</numSteps>
        <deltaT>5e-5</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <fluidMechLin formulation="compressible" presPolyId="orderPres" velPolyId="orderVel" enableGridVelC1="true" enableGridVelC2="false">
        <regionList>
          <region name="Channel" movingMeshId="moveGridID"/>
        </regionList>
        
        <movingMeshList>
          <movingMesh name="moveGridID">
            <!-- nur x -->
            <!-- comp dof="x" value="50"/-->
            <!-- y od xy -->
            <!-- comp dof="x" value="50*cos(ay)"/>
            <comp dof="y" value="0"/>
            <comp dof="z" value="-50*sin(ay)"/-->
            <!-- z od xz-->
            <!-- comp dof="x" value="50*cos(az)"/>
            <comp dof="y" value="50*sin(az)"/>
            <comp dof="z" value="0"/-->
            <!-- xyz und yz -->
            <comp dof="x" value="50*cos(ay)*cos(az)"/>
            <comp dof="y" value="50*sin(az)*cos(ay)"/>
            <comp dof="z" value="-50*sin(ay)"/>
          </movingMesh>
        </movingMeshList>
        
        <bcsAndLoads> 
          <scalarVelocityConstraint name="BC_Top" volumeRegion="Channel">
            <noPenetration/>
          </scalarVelocityConstraint>
          <scalarVelocityConstraint name="BC_Bot" volumeRegion="Channel">
            <noPenetration/>
          </scalarVelocityConstraint>
          <scalarVelocityConstraint name="BC_Front" volumeRegion="Channel">
            <noPenetration/>
          </scalarVelocityConstraint>
          <scalarVelocityConstraint name="BC_Back" volumeRegion="Channel">
            <noPenetration/>
          </scalarVelocityConstraint>
          
          <pressure name="Excite" value="sin(2*pi*1000*t)*(1-exp(-t/(3e-3)))"/>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions outputIds="h5"/>         
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions outputIds="h5"/> 
            <nodeList>
              <nodes name="S1" outputIds="txt"/>
              <nodes name="S2" outputIds="txt"/>
            </nodeList>        
          </nodeResult>
          <elemResult type="fluidMechMeshVelocityElem">
            <allRegions outputIds="h5"/>
          </elemResult>
        </storeResults>
      </fluidMechLin>
    </pdeList>
    
    <linearSystems>
      <system>
        <solverList>
          <pardiso/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
