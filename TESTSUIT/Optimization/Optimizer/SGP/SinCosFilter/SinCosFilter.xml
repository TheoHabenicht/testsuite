<cfsSimulation xmlns="http://www.cfs++.org/simulation">
  <documentation>
    <title>SGPRot3dZ</title>
    <authors>
      <author>Bich Ngoc Vu</author>
    </authors>
    <date>2022-11-30</date>
    <keywords>
      <keyword>optimization</keyword>
    </keywords>    
    <references>
      None
    </references>
    <isVerified>yes</isVerified>
    <description> 
       Test SGP optimizer with sine and cosine filtering for angles and respective output in optResult framework
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <hdf5 fileName="SinCosFilter.h5ref"/>
    </input>
    <output>
        <hdf5/>
      <info/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="3d">
    <regionList>
      <region material="99lines" name="mech"/>
    </regionList>
    <nodeList>
      <nodes name="load1">
        <list>
          <freeCoord comp="z" stop="0.5" inc="0.05" start="0"/>
          <fixedCoord comp="x" value="1.5"/>
          <fixedCoord comp="y" value="2.0"/>
        </list>
      </nodes>
      <nodes name="load2">
        <list>
          <freeCoord comp="z" stop="0.5" inc="0.05" start="0"/>
          <fixedCoord comp="x" value="1.5"/>
          <fixedCoord comp="y" value="0"/>
        </list>
      </nodes>
    </nodeList>
  </domain>
  
  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>

    <pdeList>
      <mechanic subType="3d">
        <regionList>
          <region name="mech"/>
        </regionList>

        <bcsAndLoads>
           <fix name="left_bottom_back">
              <comp dof="x"/>
              <comp dof="y"/>
              <comp dof="z"/>
           </fix>
           <fix name="left_bottom_front">
              <comp dof="x"/>
              <comp dof="y"/>
              <comp dof="z"/>
           </fix>
           <fix name="left_top_back">
              <comp dof="x"/>
              <comp dof="y"/>
              <comp dof="z"/>
           </fix>
           <fix name="left_top_front">
              <comp dof="x"/>
              <comp dof="y"/>
              <comp dof="z"/>
           </fix>
           <fix name="right_bottom_back">
              <comp dof="x"/>
              <comp dof="y"/>
              <comp dof="z"/>
           </fix>
           <fix name="right_bottom_front">
              <comp dof="x"/>
              <comp dof="y"/>
              <comp dof="z"/>
           </fix>
           <fix name="right_top_back">
              <comp dof="x"/>
              <comp dof="y"/>
              <comp dof="z"/>
           </fix>
           <fix name="right_top_front">
              <comp dof="x"/>
              <comp dof="y"/>
              <comp dof="z"/>
           </fix>
           
           <force name="load1">
             <comp dof="y" value="-1"/>
           </force>
           <force name="load2">
             <comp dof="y" value="1"/>
           </force>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
          </nodeResult>
          <elemResult type="optResult_1">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_2">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_3">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_4">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_5">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_6">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_7">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_8">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_9">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_10">
            <allRegions/>
          </elemResult>
        </storeResults>
      </mechanic>
    </pdeList>
  </sequenceStep>

  <loadErsatzMaterial file="init_0-pi2.density.xml" set="last"/>

  <optimization log="">
    <costFunction type="compliance" task="minimize" multiple_excitation="true" linear="true">
      <stopping queue="999" value="0.001" type="designChange"/>
    </costFunction>

    <constraint type="volume" value="0.3" bound="upperBound" linear="true" mode="constraint" design="density"/>

    <optimizer type="sgp" maxIterations="0">
      <sgp precomputeTensors="false" generatePlotData="false">
        
        <option name="approximation" type="string" value="asymptotes"/>
        
        <option name="levels" type="integer" value="3"/>
        
        <option name="samples_per_level_density" type="integer" value="5"/>
        <option name="samples_per_level_angle" type="integer" value="7"/>
        
        <option name="max_bisections" type="integer" value="20"/>
        
        <option name="tau_init" type="real" value="1e-6"/>
        <option name="tau_factor" type="real" value="10"/>
        
        <option name="filtering" type="string" value="non_linear"/>
        
        <option name="p_filt_density" type="real" value="0.1"/>
        <option name="p_filt_angle" type="real" value="0.2"/>
        
        <option name="pmin_vol" type="real" value="0"/>
        <option name="pmax_vol" type="real" value="10"/>
        
        <option name="tolerance" type="real" value="1e-6"/>
        
        <option name="volume_tolerance" type="real" value="1e-3"/>
      </sgp>
    </optimizer>

    <ersatzMaterial region="mech" material="mechanic" method="paramMat">
      <paramMat>
        <designMaterials>
          <designMaterial type="density-times-rotated-transversal-isotropic" isoplane="yz" rotationtype="xyz">
            <param name="emodul" value="10"/>
            <param name="emodul-iso" value="1"/>
            <param name="gmodul" value="0.5"/>
            <param name="poisson" value="0.06"/>
            <param name="poisson-iso" value="0.3"/>
            <param name="rotAngleSecond" value="0"/>
            <param name="rotAngleThird" value="0"/>
          </designMaterial>
        </designMaterials>
      </paramMat>
      
      <design name="density" initial="0.3" lower="1e-3" upper="1"/>
      <design name="rotAngleFirst" initial="0.22" lower="0" upper="3.141592653589793"/>
      
      <filters pass_to_external="true" write_mat_filt="false">
        <filter neighborhood="maxEdge" value="1.1" type="density" design="density"/>
        <filter neighborhood="maxEdge" value="1.1" type="density" design="rotAngleFirst"/>
      </filters>

      <transferFunction design="density" type="simp" application="mech" param="3.0"/>
      <result value="design" design="density" id="optResult_1"/>
      <result value="design" design="rotAngleFirst" id="optResult_2"/>
      <result value="design" design="rotAngleSecond" id="optResult_3"/>
      <result value="design" design="rotAngleThird" id="optResult_4"/>
      <result value="diffFilteredDesign" design="density" id="optResult_5"/>
      <result value="diffFilteredDesign" design="rotAngleFirst" detail="sin" id="optResult_6"/>
      <result value="diffFilteredDesign" design="rotAngleFirst" detail="cos" id="optResult_7"/>
      <result value="filteredDesign" design="density" detail="none" id="optResult_8"/>
      <result value="filteredDesign" design="rotAngleFirst" detail="sin" id="optResult_9"/>
      <result value="filteredDesign" design="rotAngleFirst" detail="cos" id="optResult_10"/>
      <export save="all" write="iteration"/>
    </ersatzMaterial>
    <commit mode="forward" stride="9999"/>
  </optimization>
</cfsSimulation>
