CylindricalWave2DMortar
=====================
- This test case solves the cylindrical wave propagation described in [CylindricalWave2D](https://gitlab.com/openCFS/Testsuite/-/tree/master/TESTSUIT/Coupledfield/LinFlowMech/CylindricalWave2D)
 using the Mortar method.
 
- COMSOL results and geometry files are provided in the above mentioned test case.

