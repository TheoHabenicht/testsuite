<cfsSimulation xmlns="http://www.cfs++.org/simulation" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org/simulation 
https://opencfs.gitlab.io/cfs/xml/CFS-Simulation/CFS.xsd">
 
  <documentation>
    <title>CompressedChamber2D</title>
    <authors>
      <author>fspoerk</author>
    </authors>
    <date>2022-05-21</date>
    <keywords>
      <keyword>mechanic</keyword>
      <keyword>flow</keyword>
      <keyword>smooth</keyword>
      <keyword>output</keyword>
    </keywords>
    <references> 
          Non Just functionality test
    </references>
    <isVerified>yes</isVerified>
    <description>
   	  This test consists of a metal plate and channel with air. The plate is excited with a pressure BC. 
   	  The mechanicPDE is coupled to the smoothPDE via Dirichlet BCs, whereas the smoothPDE is coupled to the fluidMechLinPDE. 
   	  The quantity 'fluidMechSurfaceTraction' of the fluidMechLinPDE is applied as traction BC to the mechanicPDE. 
   	  The coupling and all calculations are done iteratively until we get a convergent solution (stopping criteria defined in couplingList). 
    </description>
  </documentation>

  <fileFormats>
    <input>
      <!--cdb fileName="mech_Channel_mesh.cdb" gridId="default" id="h1"/-->
      <hdf5 fileName="CompressedChamber2D.h5ref"/>
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane">
    <variableList>
      <var name="w_plate" value="3e-4"/>
      <var name="w_air" value="1e-3"/>
      <var name="h" value="1e-3"/>
    </variableList>
    
    <regionList>
      <region name="plate" material="silizium"/>
      <region name="Air" material="FluidMat"/>
    </regionList>
    
    <surfRegionList>
      <surfRegion name="ExcPre"/>
      <surfRegion name="Fix_t"/>
      <surfRegion name="Fix_b"/>
      <surfRegion name="PlateAirCoupling"/>
      <surfRegion name="Fix_Air_t"/>
      <surfRegion name="Fix_Air_b"/>
      <surfRegion name="Fix_Air_r"/>
    </surfRegionList>
    
    <nodeList>
      <nodes name="S1">
        <coord x="w_plate" y="h/2" z="0"/>
      </nodes>
    </nodeList>
  </domain>
  
  <fePolynomialList>
    <!-- Set second order polynomial for velocity -->
    <Lagrange id="velPolyId">
      <isoOrder>2</isoOrder> 
    </Lagrange>
    
    <!-- Set first order polynomial for pressure -->
    <Lagrange id="presPolyId">
      <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>
  
  <sequenceStep index="1"> 
    <analysis>
      <transient>
        <numSteps>15</numSteps>
        <deltaT>2e-5</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <mechanic subType="planeStrain">
      	<regionList>
          <region name="plate"/>
        </regionList>
        
        <bcsAndLoads>
          <fix name="Fix_b">
            <comp dof="y"/>
          </fix>
          <fix name="Fix_t">
            <comp dof="y"/>
          </fix>
          <pressure name="ExcPre" value="10*(1-exp(-t/(2e-5)))"/>
          <traction name="PlateAirCoupling">
            <coupling pdeName="fluidMechLin">
            	<quantity name="fluidMechSurfaceTraction"/>
            </coupling>
          </traction>
        </bcsAndLoads>
         
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
          </nodeResult>
          <surfElemResult type="mechNormalStress">
          </surfElemResult>
        </storeResults>
      </mechanic>
      
      <smooth subType="planeStrain">
        <regionList>
          <region name="Air"/>
        </regionList>
        <bcsAndLoads>
          <fix name="Fix_Air_t">
            <comp dof="y"/>
          </fix>
          <fix name="Fix_Air_b">
            <comp dof="y"/>
          </fix>
          <fix name="Fix_Air_r">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <displacement name="PlateAirCoupling">
            <coupling pdeName="mechanic">
              <quantity name="mechDisplacement"/>
            </coupling>
          </displacement>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions/>
            <nodeList>
              <nodes name="S1" outputIds="txt"/>
            </nodeList>
          </nodeResult>
          <nodeResult type="smoothVelocity">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </smooth>

      <fluidMechLin formulation="compressible" velPolyId="velPolyId" presPolyId="presPolyId" enableGridVelC1="true" enableGridVelC2="true">
        <regionList>
          <region name="Air" movingMeshId="moveGridID"/>
        </regionList>
        
        <movingMeshList>
          <movingMesh name="moveGridID">
            <coupling pdeName="smooth">
              <quantity name="smoothVelocity"/>
            </coupling>
          </movingMesh>
        </movingMeshList>
        
        <bcsAndLoads>
          <noSlip name="Fix_Air_t">  
            <comp dof="y"/>
          </noSlip>
          <noSlip name="Fix_Air_b">    
            <comp dof="y"/>
          </noSlip>
          <noSlip name="Fix_Air_r">
            <comp dof="x"/>
            <comp dof="y"/>
          </noSlip>
          <velocity name="PlateAirCoupling">
            <coupling pdeName="smooth">
              <quantity name="smoothVelocity"/>
            </coupling>
          </velocity>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions outputIds="h5"/>
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions outputIds="h5"/>
            <nodeList>
              <nodes name="S1" outputIds="txt"/>
            </nodeList>
          </nodeResult>
          <elemResult type="fluidMechMeshVelocityElem">
            <allRegions outputIds="h5"/>
          </elemResult>
          <surfElemResult type="fluidMechSurfaceTraction">
            <surfRegionList>
              <surfRegion name="PlateAirCoupling"/>
            </surfRegionList>
          </surfElemResult>
        </storeResults>
      </fluidMechLin>
    </pdeList>
    
    <couplingList>
      <iterative PDEorder="mechanic;smooth;MechLinFlow">
        <convergence logging="yes" maxNumIters="20" stopOnDivergence="no">
          <quantity name="smoothDisplacement" value="1e-3" normType="rel"/>
          <quantity name="smoothVelocity" value="1e-3" normType="rel"/>
          <quantity name="fluidMechVelocity" value="1e-3" normType="rel"/>
          <quantity name="fluidMechPressure" value="1e-3" normType="rel"/>
          <quantity name="mechDisplacement" value="1e-3" normType="rel"/>
          <quantity name="mechVelocity" value="1e-3" normType="rel"/>
        </convergence>
        <geometryUpdate>
          <region name="Air"/>
        </geometryUpdate>
      </iterative>
    </couplingList>
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <matrix storage="sparseNonSym"/>
          </standard>
        </solutionStrategy>
        <solverList>
          <pardiso/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
  
</cfsSimulation>
